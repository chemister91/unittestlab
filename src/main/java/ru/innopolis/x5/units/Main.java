package ru.innopolis.x5.units;

import ru.innopolis.x5.units.dao.StudentDao;
import ru.innopolis.x5.units.dao.StudentDaoSimpleImpl;
import ru.innopolis.x5.units.model.Group;
import ru.innopolis.x5.units.model.Lesson;
import ru.innopolis.x5.units.model.Student;
import ru.innopolis.x5.units.model.Visit;
import ru.innopolis.x5.units.service.StudentService;

public class Main {
    public static void main(String[] args) {
        Group group = new Group(1, "The first group");
        Lesson lesson = new Lesson(1, "The first lesson", group);
        Student petrov = new Student("Petrov A.B.");
        Student abramov = new Student("Abramov A.A.");

        StudentDao studentDao = new StudentDaoSimpleImpl();
        studentDao
                .addGroup(group)
                .addLesson(lesson)
                .addStudent(petrov)
                .addStudent(abramov)
                .addToGroup(group, petrov)
                .addToGroup(group, abramov)
                .addVisit(lesson, new Visit(petrov, 5))
                .addVisit(lesson, new Visit(petrov, 4))
                .addVisit(lesson, new Visit(abramov, 3))
                .addVisit(lesson, new Visit(abramov, 6));

        StudentService studentService = new StudentService(studentDao);

        System.out.println(studentService.getMeanGrade(lesson.getId()));
        System.out.println(studentService.getStudentWithVowelNames());
    }
}
