package ru.innopolis.x5.units.service.mapper;

import ru.innopolis.x5.units.dao.StudentDao;
import ru.innopolis.x5.units.model.Group;
import ru.innopolis.x5.units.model.Lesson;
import ru.innopolis.x5.units.model.Student;
import ru.innopolis.x5.units.model.Visit;
import ru.innopolis.x5.units.outermodel.IncomingVisit;

public class IncomingVisitToLessonMapperImpl implements IncomingVisitToLessonMapper {
    private StudentDao studentDao;

    public IncomingVisitToLessonMapperImpl(StudentDao studentDao) {
        this.studentDao = studentDao;
    }

    @Override
    public Lesson mapIncomingVisitToLesson(IncomingVisit incomingVisit) {
        Group group = new Group(
                incomingVisit.getGroupID(),
                incomingVisit.getGroupName()
        );
        Lesson lesson = new Lesson(
                incomingVisit.getLessonID(),
                incomingVisit.getLessonTopic(),
                group
        );
        Student student = new Student(
                incomingVisit.getStudentID(),
                incomingVisit.getStudentFio(),
                incomingVisit.getGroupID()
        );
        Visit visit = new Visit(
                student,
                incomingVisit.getVisitGrade()
        );
        return studentDao
                .addGroup(group)
                .addLesson(lesson)
                .addStudent(student)
                .addToGroup(group, student)
                .addVisit(lesson, visit)
                .getLesson(lesson.getId());

    }
}
