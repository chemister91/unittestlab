package ru.innopolis.x5.units.service.mapper;

import ru.innopolis.x5.units.model.Lesson;
import ru.innopolis.x5.units.outermodel.IncomingVisit;

public interface IncomingVisitToLessonMapper {
    Lesson mapIncomingVisitToLesson(IncomingVisit incomingVisit);
}
