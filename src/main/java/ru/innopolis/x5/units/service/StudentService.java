package ru.innopolis.x5.units.service;

import ru.innopolis.x5.units.dao.StudentDao;
import ru.innopolis.x5.units.model.Lesson;
import ru.innopolis.x5.units.model.Student;
import ru.innopolis.x5.units.model.Visit;
import ru.innopolis.x5.units.outermodel.IncomingVisit;
import ru.innopolis.x5.units.service.mapper.IncomingVisitToLessonMapper;
import ru.innopolis.x5.units.service.mapper.IncomingVisitToLessonMapperImpl;

import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import static java.util.stream.Collectors.toList;

public class StudentService {
    private Logger logger = Logger.getLogger(this.getClass().getName());
    private StudentDao studentDao;

    public StudentService() {
    }

    public StudentService(StudentDao studentDao) {
        this.studentDao = studentDao;
    }

    public StudentDao getStudentDao() {
        return studentDao;
    }

    public void setStudentDao(StudentDao studentDao) {
        this.studentDao = studentDao;
    }

    public double getMeanGrade(int lessonId) {
        try {
            return studentDao
                    .getLesson(lessonId)
                    .getVisits()
                    .stream()
                    .mapToDouble(Visit::getGrade)
                    .average()
                    .orElse(0d);
        } catch (Throwable e) {
            logger.warning(e.toString());
            return 0;
        }
    }

    public List<String> getStudentsByPattern(String pattern) {
        try {
            return studentDao
                    .getAllLessons()
                    .stream()
                    .map(Lesson::getGroup)
                    .flatMap(group -> group.getStudents().stream())
                    .map(Student::getFio)
                    .filter(student -> student.matches(pattern))
                    .distinct()
                    .collect(toList());
        } catch (Throwable e) {
            logger.warning(e.toString());
            return Collections.emptyList();
        }
    }

    public List<String> getStudentWithVowelNames() {
        return getStudentsByPattern("(?i)^[aAeEiIoOuUyY].*$");
    }

    public StudentService loadIncomingVisit(IncomingVisit incomingVisit) {
        IncomingVisitToLessonMapper incomingVisitToLessonMapper = new IncomingVisitToLessonMapperImpl(studentDao);
        incomingVisitToLessonMapper.mapIncomingVisitToLesson(incomingVisit);
        return this;
    }
}
