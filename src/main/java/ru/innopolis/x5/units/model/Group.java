package ru.innopolis.x5.units.model;

import java.util.*;

public class Group {
    private Integer id;
    private String name;
    private List<Student> students = new ArrayList<>();

    public Group() {
    }

    public Group(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Student> getStudents() {
        if (students == null) {
            return Collections.emptyList();
        } else {
            return students;
        }
    }

    public Group addStudent(Student student) {
        if (student != null && !students.contains(student)) {
            student.setGroup(id);
            students.add(student);
        }
        return this;
    }

    @Override
    public String toString() {
        return "ru.innopolis.x5.units.model.Group{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
