package ru.innopolis.x5.units.model;

public class Visit {
    private Student student;
    private Integer grade;

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Visit() {
    }

    public Visit(Student student, Integer grade) {
        this.student = student;
        this.grade = grade;
    }
}
