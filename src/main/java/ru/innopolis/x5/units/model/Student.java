package ru.innopolis.x5.units.model;

public class Student {
    private Integer id;
    private String fio;
    private Integer groupID;

    public Student() {
    }

    public Student(String fio) {
        this.fio = fio;
    }

    public Student(Integer id, String fio, Integer groupID) {
        this.id = id;
        this.fio = fio;
        this.groupID = groupID;
    }

    public Integer getId() {
        if (id == null) {
            return 0;
        } else {
            return id;
        }
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public Integer getGroup() {
        return groupID;
    }

    public void setGroup(Integer group) {
        this.groupID = group;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", fio='" + fio + '\'' +
                ", group=" + groupID +
                '}';
    }
}
