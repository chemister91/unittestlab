package ru.innopolis.x5.units.dao;

import ru.innopolis.x5.units.model.*;

import java.util.*;
import java.util.logging.Logger;

public class StudentDaoSimpleImpl implements StudentDao {
    private static Map<Integer, Lesson> lessonMap = new HashMap<>();
    private static Map<Integer, Group> groupMap = new HashMap<>();
    private static Map<Integer, Student> studentMap = new HashMap<>();
    private Logger logger = Logger.getLogger(this.getClass().getName());

    @Override
    public List<Lesson> getAllLessons() {
        return new ArrayList<>(lessonMap.values());
    }

    @Override
    public List<Group> getAllGroups() {
        return new ArrayList<>(groupMap.values());
    }

    @Override
    public List<Student> getAllStudents() {
        return new ArrayList<>(studentMap.values());
    }

    @Override
    public Lesson getLesson(Integer lessonId) {
        if (lessonMap.containsKey(lessonId)) {
            return lessonMap.get(lessonId);
        } else {
            throw new IllegalArgumentException(LESSON_DOES_NOT_EXIST);
        }
    }

    @Override
    public StudentDao addLesson(String topic, Integer groupID) {
        return addLesson(new Lesson(topic, getGroup(groupID)));
    }

    @Override
    public StudentDao addLesson(Lesson lesson) {
        if (lessonMap.containsKey(lesson.getId())) {
            logger.warning(SKIP_PREF + LESSON_ALREADY_EXISTS);
        } else {
            lessonMap.put(getLessonID(lesson), lesson);
        }
        return this;
    }

    @Override
    public StudentDao addVisit(Integer lessonID, Student student, Integer grade) {
        return addVisit(getLesson(lessonID), new Visit(student, grade));
    }

    @Override
    public StudentDao addVisit(Lesson lesson, Visit visit) {
        int lessonID = getLessonID(lesson);
        if (!lessonMap.containsKey(lessonID)) {
            addLesson(lesson);
        }
        lessonMap.get(lessonID).addVisit(visit);
        return this;
    }

    @Override
    public StudentDao addGroup(Integer id, String name) {
        return addGroup(new Group(id, name));
    }

    @Override
    public StudentDao addGroup(Group group) {
        if (groupMap.containsKey(group.getId())) {
            logger.warning(SKIP_PREF + GROUP_ALREADY_EXISTS);
        } else {
            groupMap.put(group.getId(), group);
        }
        return this;
    }

    @Override
    public Group getGroup(Integer groupID) {
        if (groupMap.containsKey(groupID)) {
            return groupMap.get(groupID);
        } else {
            throw new IllegalArgumentException(GROUP_DOES_NOT_EXIST);
        }
    }

    @Override
    public StudentDao addToGroup(Group group, Student student) {
        return addToGroup(group.getId(), student.getId());
    }

    @Override
    public StudentDao addToGroup(Integer groupID, Integer studentID) {
        Student student;
        if (studentMap.containsKey(studentID)) {
            student = studentMap.get(studentID);
        } else {
            throw new IllegalArgumentException(STUDENT_DOES_NOT_EXIST);
        }
        getGroup(groupID).addStudent(student);
        return this;
    }

    @Override
    public StudentDao addStudent(Student student) {
        if (studentMap.containsKey(student.getId())) {
            logger.warning(SKIP_PREF + STUDENT_ALREADY_EXISTS);
        } else {
            studentMap.put(getStudentID(student), student);
        }
        return this;
    }


    private int getLessonID(Lesson lesson) {
        if (lesson.getId().equals(0)) {
            lesson.setId(getIdentity(lessonMap.keySet()));
        }
        return lesson.getId();
    }

    private int getStudentID(Student student) {
        if (student.getId().equals(0)) {
            student.setId(getIdentity(studentMap.keySet()));
        }
        return student.getId();
    }

    private int getIdentity(Set<Integer> keySet) {
        return keySet
                .stream()
                .mapToInt(Integer::intValue)
                .max()
                .orElse(0)
                + 1;
    }
}

