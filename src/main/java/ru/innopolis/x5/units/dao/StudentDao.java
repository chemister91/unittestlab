package ru.innopolis.x5.units.dao;

import ru.innopolis.x5.units.model.Group;
import ru.innopolis.x5.units.model.Lesson;
import ru.innopolis.x5.units.model.Student;
import ru.innopolis.x5.units.model.Visit;

import java.util.List;

public interface StudentDao {
    String SKIP_PREF = "Adding skipped: ";
    String GROUP_ALREADY_EXISTS = "This group number already exists";
    String GROUP_DOES_NOT_EXIST = "This group does not exist";
    String LESSON_ALREADY_EXISTS = "This lesson already exists";
    String LESSON_DOES_NOT_EXIST = "This lesson does not exist";
    String STUDENT_ALREADY_EXISTS = "This student already exists";
    String STUDENT_DOES_NOT_EXIST = "This student does not exist";

    List<Lesson> getAllLessons();

    List<Group> getAllGroups();

    List<Student> getAllStudents();

    Lesson getLesson(Integer lessonId);

    StudentDao addLesson(String topic, Integer groupID);

    StudentDao addLesson(Lesson lesson);

    StudentDao addVisit(Integer lessonID, Student student, Integer grade);

    StudentDao addVisit(Lesson lesson, Visit visit);

    StudentDao addGroup(Integer id, String name);

    StudentDao addGroup(Group group);

    Group getGroup(Integer groupID);

    StudentDao addToGroup(Group group, Student student);

    StudentDao addToGroup(Integer groupID, Integer studentID);

    StudentDao addStudent(Student student);
}
