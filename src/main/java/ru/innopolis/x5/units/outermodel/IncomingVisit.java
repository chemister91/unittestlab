package ru.innopolis.x5.units.outermodel;

public class IncomingVisit {
    private Integer studentID;
    private String studentFio;
    private Integer groupID;
    private String groupName;
    private Integer lessonID;
    private String lessonTopic;
    private Integer visitGrade;

    public IncomingVisit(Integer studentID, String studentFio, Integer groupID, String groupName, Integer lessonID, String lessonTopic, Integer visitGrade) {
        this.studentID = studentID;
        this.studentFio = studentFio;
        this.groupID = groupID;
        this.groupName = groupName;
        this.lessonID = lessonID;
        this.lessonTopic = lessonTopic;
        this.visitGrade = visitGrade;
    }

    public Integer getStudentID() {
        return studentID;
    }

    public String getStudentFio() {
        return studentFio;
    }

    public Integer getGroupID() {
        return groupID;
    }

    public String getGroupName() {
        return groupName;
    }

    public Integer getLessonID() {
        return lessonID;
    }

    public String getLessonTopic() {
        return lessonTopic;
    }

    public Integer getVisitGrade() {
        return visitGrade;
    }
}
