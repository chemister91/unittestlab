package ru.innopolis.x5.units.service;

import org.junit.Test;
import org.junit.Before;
import org.mockito.Mock;
import ru.innopolis.x5.units.dao.StudentDao;
import ru.innopolis.x5.units.model.Visit;

import java.util.Collections;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;


public class StudentServiceTest {
    private StudentService studentService;
    @Mock
    private StudentDao studentDao;

    @Before
    public void setUp() {
        initMocks(this);
        this.studentService = new StudentService(studentDao);
    }

    @Test
    public void meanGradeDaoReturnNull() {
        when(studentDao.getLesson(any())).thenReturn(null);
        double result = studentService.getMeanGrade(1);
        assertEquals(0d, result);
    }

    @Test
    public void meanGradeNoGrade() {
        TestHelper testHelper = new TestHelper();
        when(studentDao.getLesson(1))
                .thenReturn(testHelper.getLesson1());
        double result = studentService.getMeanGrade(1);
        assertEquals(0d, result);
    }

    @Test
    public void meanGradeHaveGrade() {
        TestHelper testHelper = new TestHelper();
        when(studentDao.getLesson(1))
                .thenReturn(testHelper.getLesson1()
                        .addVisit(new Visit(testHelper.getAbramov(), 5))
                        .addVisit(new Visit(testHelper.getPetrov(), 3))
                );
        double result = studentService.getMeanGrade(1);
        assertEquals(4d, result);
    }

    @Test
    public void studentByPatternNoLessons() {
        when(studentDao.getAllLessons())
                .thenReturn(Collections.EMPTY_LIST);
        assertEquals(Collections.EMPTY_LIST, studentService.getStudentWithVowelNames());
    }

    @Test
    public void studentByPatternNoGroups() {
        TestHelper testHelper = new TestHelper();
        when(studentDao.getAllLessons())
                .thenReturn(testHelper.getLessonListWithoutGroup());
        assertEquals(Collections.EMPTY_LIST, studentService.getStudentWithVowelNames());
    }

    @Test
    public void studentByPatternNoStudents() {
        TestHelper testHelper = new TestHelper();
        when(studentDao.getAllLessons())
                .thenReturn(testHelper.getLessonListWithoutStudents());
        assertEquals(Collections.EMPTY_LIST, studentService.getStudentWithVowelNames());
    }

    @Test
    public void studentByPatternNoName() {
        TestHelper testHelper = new TestHelper();
        when(studentDao.getAllLessons())
                .thenReturn(testHelper.getLessonListWithNoNameStudents());
        assertEquals(Collections.EMPTY_LIST, studentService.getStudentWithVowelNames());
    }

    @Test
    public void studentByPatternVowelName() {
        TestHelper testHelper = new TestHelper();
        when(studentDao.getAllLessons())
                .thenReturn(testHelper.getLessonList());
        assertEquals(testHelper.getVowelStudents(), studentService.getStudentWithVowelNames());
    }

    @Test
    public void testIncomingVisit() {
        TestHelper testHelper = new TestHelper();
        StudentDao testStudentDao = testHelper.getStudentDaoSimpleImpl();
        StudentService testStudentService = new StudentService(testStudentDao);
        testStudentService
                .loadIncomingVisit(testHelper.getGuestIncomingVisit(15))
                .loadIncomingVisit(testHelper.getGuestIncomingVisit(5))
                .loadIncomingVisit(testHelper.getBestIncomingVisit(7))
                .loadIncomingVisit(testHelper.getTestIncomingVisit(8));
        assertEquals(3, testStudentDao.getAllLessons().size());
        assertEquals(3, testStudentDao.getAllStudents().size());
        assertEquals(3, testStudentDao.getAllGroups().size());
        assertEquals(10d, testStudentService.getMeanGrade(22));
        assertEquals(3, testStudentService.getStudentsByPattern(".*").size());
    }
}