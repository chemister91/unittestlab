package ru.innopolis.x5.units.service;

import ru.innopolis.x5.units.dao.StudentDao;
import ru.innopolis.x5.units.dao.StudentDaoSimpleImpl;
import ru.innopolis.x5.units.model.Group;
import ru.innopolis.x5.units.model.Lesson;
import ru.innopolis.x5.units.model.Student;
import ru.innopolis.x5.units.outermodel.IncomingVisit;

import java.util.ArrayList;
import java.util.List;

class TestHelper {
    private Group group1;
    private Lesson lesson1;
    private Lesson lesson2;
    private Student abramov;
    private Student petrov;
    private List<Lesson> lessonList = new ArrayList<>();

    public TestHelper() {
        group1 = new Group(1, "The first group");
        petrov = new Student("Petrov A.B.");
        abramov = new Student("Abramov A.A.");
        group1.addStudent(petrov);
        group1.addStudent(abramov);
        lesson1 = new Lesson("The first lesson", group1);
        lesson2 = new Lesson("The second lesson", group1);
        lessonList.add(lesson1);
        lessonList.add(lesson2);
    }

    public Group getGroup1() {
        return group1;
    }

    public Lesson getLesson1() {
        return lesson1;
    }

    public Student getAbramov() {
        return abramov;
    }

    public Student getPetrov() {
        return petrov;
    }

    public List<Lesson> getLessonList() {
        return lessonList;
    }

    public List<Lesson> getLessonListWithoutGroup() {
        List<Lesson> lessonList = new ArrayList<>();
        lessonList.add(new Lesson());
        return lessonList;
    }

    public List<Lesson> getLessonListWithoutStudents() {
        List<Lesson> lessonList = new ArrayList<>();
        lessonList.add(new Lesson(
                "No students",
                new Group(6, "Empty group"))
        );
        return lessonList;
    }

    public List<Lesson> getLessonListWithNoNameStudents() {
        List<Lesson> lessonList = new ArrayList<>();
        lessonList.add(new Lesson(
                "No name students",
                new Group(7, "Anonymous group")
                        .addStudent(new Student())
                        .addStudent(new Student("")))
        );
        return lessonList;
    }

    public List<String> getVowelStudents() {
        List<String> studentList = new ArrayList<>();
        studentList.add(abramov.getFio());
        return studentList;
    }

    public StudentDao getStudentDaoSimpleImpl() {
        return new StudentDaoSimpleImpl();
    }

    public IncomingVisit getTestIncomingVisit(Integer visitGrade) {
        return new IncomingVisit(1, "Test T.T.", 101, "Test group", 11, "Test lesson", visitGrade);
    }

    public IncomingVisit getGuestIncomingVisit(Integer visitGrade) {
        return new IncomingVisit(2, "Guest G.G.", 202, "Guest group", 22, "Guest lesson", visitGrade);
    }

    public IncomingVisit getBestIncomingVisit(Integer visitGrade) {
        return new IncomingVisit(3, "Best B.B.", 303, "Best group", 33, "Best lesson", visitGrade);
    }
}
